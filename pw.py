#!/usr/bin/env python
import random
from urllib.request import urlopen

lines = urlopen(
    "https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english-no-swears.txt"
)

words = list({
    line.decode("utf8").strip()
    for line in lines
    if len(line) > 6
})
print("Unique words:", len(words))
print("Possible passwords ~", len(words) ** 4)

for i in range(5):
    print("-".join(random.sample(words, 4)))