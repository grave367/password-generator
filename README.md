# Password Generator



## What is this

This is sanitized backend code for a customer project.

The customer had requested an interface where users are given a selection of passwords (as compared to coming up with their own)

The selected password was propagated across their internal domain using Microsoft Identity Manager's PAM (Password Access Management) utility.

In the customer's context, this was their workflow
- Employee is served a webpage on their first login with their domain account
- They were served a SharePoint website where they had a seed value pulled from their profile (Randomly selected from entries like address, phone number, etc)
- Seed value is used in generation from word list
- Employee would select a generated password that they would use.
- Password would propagate across the domain

In the context of this script
- Run pw.py
- Script selects 4 words from a google hosted no swear word list
- Script prints 5 unique passwords


## Example

- Running python pw.py output



Unique words: 6343

Possible passwords ~ 1618746511855201

winners-regulation-relatively-amplifier

impression-absolutely-scheme-completing

system-branch-pichunter-configuring

stopping-disposition-stability-houses


pulled-evaluating-prepare-huntington

